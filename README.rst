OpenCM3 Template 
================

This project is a template for libOpenCM3 projects based on `lipOpenCM3-template <https://github.com/libopencm3/libopencm3-template/tree/master>`_ . It includes a development container configuration for VSCode and a Makefile for building and flashing the firmware. 


Devcontainer
------------

To use the Devcontainer, open the project in VSCode and select `Reopen in Container` from the command palette. This will build the container and open the project in it.



Building
--------

To build the project, run `make` in the project root. This will build the firmware and place the resulting binary in `blink.elf`.

